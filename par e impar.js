
function filtrarNumeros(arr, tipo) {
    if (tipo === 'par') {
        return arr.filter(num => num % 2 === 0);
    } else if (tipo === 'impar') {
        return arr.filter(num => num % 2 !== 0);
    } else {
        return [];
    }
}

function factorial(numero) {
    if (numero === 0 || numero === 1) {
        return 1;
    } else {
        return numero * factorial(numero - 1);
    }
}

function contarNumerosRepetidos(arr) {
    let repetidos = {};
    let numerosRepetidos = [];

    arr.forEach(num => {
        if (repetidos[num]) {
            if (repetidos[num] === 1) {
                numerosRepetidos.push(num);
            }
            repetidos[num]++;
        } else {
            repetidos[num] = 1;
        }
    });

    return numerosRepetidos.length;
}


// Filtrar números pares o impares
const numeros = [1, 2, 3, 4, 5];
const tipo = 'par';
const numerosFiltrados = filtrarNumeros(numeros, tipo);
console.log(numerosFiltrados);

// Calcular el factorial de un número
const num = 5;
const resultadoFactorial = factorial(num);
console.log(resultadoFactorial);

// Contar números repetidos en un arreglo
const numerosRepetidos = [3, 4, 7, 8, 1, 2, 3, 3, 1];
const cantidadRepetidos = contarNumerosRepetidos(numerosRepetidos);
console.log(`${cantidadRepetidos}, se repiten ciertos números.`);
