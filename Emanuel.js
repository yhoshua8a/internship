function getType(type, number) {
    if (type === "par") {
        const pares = number.filter(number => number % 2 === 0);
        return pares;
    } else if (type === "impar") {
        const impares = number.filter(number => number % 2 != 0);
        return impares;
    }
}

function getFactorial(number) {
    const array = [];
    for (let i = 1; i < number + 1; i++) {
        array.push(i);
    }
    const Fact = array.reduce((acumulador, currentValue) => acumulador * currentValue);
    return Fact;
}

function getRep(number) {

    const repetidos = [];
    for (let i = 0; i < number.length ; i++){
      for (let j = i + 1; j < number.length; j++) {
      if (number[i] === number[j + 1] && !repetidos.includes(number[i]) )  {
        repetidos.push(number[i]);
        }
      }
    }
    console.log("Hay " + repetidos.length + " numeros repetidos, ");
    return repetidos;
  }

  console.log(getType("par", [1, 4, 8, 3, 7, 10]));
  console.log(getType("impar", [1, 4, 8, 3, 7]));
  console.log(getFactorial(5));
  console.log(getRep([3, 4, 7, 8, 1, 2, 3, 3, 3, 1]));